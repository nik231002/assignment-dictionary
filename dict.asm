section .text

global find_word

extern string_length
extern string_equals


find_word:
.loop:
    push rdi            
    push rsi            
    add rsi, 8          
    call string_equals  
    pop rsi             
    pop rdi     
    cmp rax, 0
    jnz .ok
    mov rsi, [rsi]     
    cmp rsi, 0
    jnz .loop           
    xor rax, rax        
    ret   
.ok:
    mov rax, rsi        
    ret                  
