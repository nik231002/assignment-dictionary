section .rodata

%include 'colon.inc'
%include 'words.inc'
%include 'lib.inc'

section .text
section .data
err_cdntread: 
    db 'The word couldnt be read', 10, 0
err_notfound: 
    db 'The key was not found', 10, 0


section .text

global _start



_start:
    sub rsp, 256
    mov rdi, rsp
    mov rsi, 256
    call read_word
    test rax, rax
    jz .error
    mov rdi, rsp
    mov rsi, k
    call find_word
    add rsp, 256
    test rax, rax
    jz .not_found
   
    add rax, 8
    mov rdi, rax
    push rax
    call string_length
    pop rdi
    add rdi, rax
    inc rdi
    mov rsi, rdi
    mov rdi, 1
    call print_string
    call print_newline
    call exit

.error:
    add rsp, 256
    mov rsi, err_cdntread
    mov rdi, 2
    call print_string
    call exit

.not_found:
    mov rsi, err_notfound
    mov rdi, 2
    call print_string
    call exit
